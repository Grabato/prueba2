package test.inacap.prueba2.Vista;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import test.inacap.prueba2.R;

public class InicioSesionFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public InicioSesionFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_inicio_sesion, container, false);

        final EditText etNombreUsuario = (EditText) layout.findViewById(R.id.etNombreUsuario);
        final EditText etContrasena = (EditText) layout.findViewById(R.id.etContrasena);
        final Button btIniciarSesion = (Button) layout.findViewById(R.id.btIniciarSesion);

        btIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = etNombreUsuario.getText().toString().trim();
                String pass = etContrasena.getText().toString();


                if (etNombreUsuario.getText().toString().trim().isEmpty())
                    etNombreUsuario.setError("Campo Vacio");

                if (etContrasena.getText().toString().trim().isEmpty())
                    etContrasena.setError("Campo Vacio");

                if (user.isEmpty()){

                }else {
                    if (pass.length() !=0)
                        Toast.makeText(getContext(), "Contraseña: " + pass, Toast.LENGTH_SHORT).show();
                }
            }
        });

        return layout;
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("","");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(String nombreFragmento, String evento);
    }
}
