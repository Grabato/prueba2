package test.inacap.prueba2.Vista;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import test.inacap.prueba2.R;

public class DatosPersonalesFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public DatosPersonalesFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_datos_personales, container, false);

        final EditText etNombre = (EditText) layout.findViewById(R.id.etNombre);
        final EditText etEdad = (EditText) layout.findViewById(R.id.etEdad);
        final Button btSiguiente = (Button) layout.findViewById(R.id.btSiguienteDP);

        btSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nombre = etNombre.getText().toString();

                if (etNombre.getText().toString().trim().isEmpty())
                    etNombre.setError("Campo Vacio");

                else if (nombre.length() != 0){
                    mListener.onFragmentInteraction("DatosPersonalesFragment", "BTN_SIGUIENTE_CLICK");
                }
            }
        });

        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("","");
        }
    }

    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(String nombreFragmento, String evento);
    }
}

